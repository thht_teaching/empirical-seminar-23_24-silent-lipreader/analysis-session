#%% imports
import pandas as pd
from pathlib import Path
import hashlib

#%% load
df = pd.read_excel('/home/th/Downloads/attw_lipreading.xls')
# %% hash subject ids
df['Subject ID'] = df['Subject ID'].apply(lambda x: hashlib.sha256(x.encode()).hexdigest()[:8])

#%% save
df.to_excel('data/socio_demo.xlsx')
# %%
