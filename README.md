# How to prepare your computer for the analysis session

Please follow these instructions and download everything before you come to the class so we all have a smooth experience.

I tried to make the process as smooth as possible. However, if you have problems, just contact me on monday and we sort it out.

These instructions should work for Windows, Mac and Linux. If you use Linux, I assume that you have some more in-depth knowledge so I might not go into that much detail here...

You are going to need ~8GB of free disk space to do this...

# Help! I tried everything and it failed! Or my computer is not powerful enough!

There is one alternative: Run everything in the cloud. This is free but comes with some downsides:

1. You cannot save anything
2. You only have 50h per months
3. You need to create an free account at https://gitlab.com and you need a valid mobile phone number and they might ask for a credit card number (but they will not charge you anything)

I strongly suggest running everything on your computer locally. However, if you want to take this route, here is what you need to do:

1. [Register here](https://gitlab.com/users/sign_up)
2. [Sign in here](https://gitlab.com/users/sign_in)
3. Go to this website: https://gitlab.com/thht_teaching/empirical-seminar-23_24-silent-lipreader/analysis-session
4. Find this bar: ![](img/gitlab_project_header.png)
5. To the right, you see the button that reads "Web IDE".
6. Click the downward arrow next to it. **DO NOT CLICK ON "Web IDE"!!!**
7. Now choose "Gitpod"
8. The button should now say "Gitpod"
9. Click on "Gitpod"
10. Follow all instructions, say yes to everything, use all defaults
11. Wait.... for like 15min until you see a message like this:
      ```
      done
      #
      # To activate this environment, use
      #
      #     $ conda activate /home/th/git/code_for_teaching/dntmce_analysis/.venv
      #
      # To deactivate an active environment, use
      #
      #     $ conda deactivate
      ```

## Before we begin

Some of the steps require you to work in a so-called "Terminal" or on the "Command Line". This might be a concept you are unfamiliar with, but it is not that
hard. You basically have a window in which you can enter "commands". These commands might have some so-called parameters that tell the command what to do and how to do it.

A command might look like this:

```bash
echo "Hello World"
```

In this case `echo` is the command and `"Hello World"` is the parameter.

Whenever you need to enter commands in this session, all you basically need to do is to copy them as they are written here.

Only pay attention to these points:

1. Make sure to copy-paste them because small changes might make the command fail.
2. Especially important are spaces and dashes. Make sure to keep them.
3. It's all case sensitive. So `Echo` is something different than `echo`!

### How do I open this Terminal?

On Windows, we will work with the so-called `git bash` Terminal. This will only be available after you have installed git in the next step.

On Mac, you actually need the Terminal to install some software. But luckily, there is an app called `Terminal` already installed. Just open it like any other application and you get a window with a command prompt and you can start typing commands.

## Step 0
If you are already familiar with anaconda python environments and maybe even git:

1. clone this repository. be sure to have installed git-lfs!
2. be sure you have anaconda, miniconda or mamabforge installed
3. create the environment
4. launch VS code, be sure to have the python and jupyter extensions installed.
5. enjoy!

If the above is all gibberish to you, go to Step 1!

## Step 1: Download and install the necessary software

### Git

#### Windows
Go [here](https://git-scm.com/download/windows) and choose the `64-bit Git for Windows` option. When the download is complete, install it and just leave all the standard options.

#### Mac
1. Open the Terminal
2. `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
3. Close the Terminal
4. Open the Terminal again
5. `brew install git git-lfs`

> :warning: **Be sure to install both `git` and `git-lfs`!**

### Miniforge
#### Windows
1. Download [this](https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Windows-x86_64.exe) and install it keeping all the standard options.
2. Open the `Git Bash` from the Start Menu
3. `~/miniforge3/condabin/mamba.bat init bash`
4. Close the `Git Bash` Window **This is important!!**

#### Mac
1. Open the Terminal (or enter it if it is still open)
2. `brew install --cask miniforge`
3. `conda init "$(basename "${SHELL}")"`
4. Close the Terminal **This is important!!**

### Visual Studio Code
We are going to use Visual Studio Code in this tutorial. Please head over to https://code.visualstudio.com/ and download it. Then install it. You can choose the default options.

After you have opened it, you need to install some so-called extensions. To do this, click on the button that looks like this: ![](img/extension_button.png)

You need to install the following extensions:

1. Jupyter
2. Python

## Step 2: Get the code and the data
Now it is time to get the code and the data. Luckily, VS Code makes this really easy for you

1. Go to this website: https://gitlab.com/thht_teaching/empirical-seminar-23_24-silent-lipreader/analysis-session
2. At the right, you are going to see a button called "Clone".
   1. Click that one
   2. Then choose the option: "Visual Studio Code (HTTPS)"
3. The browser is going to ask you whether Visual Studio Code is allowed to open the link. Click yes.
4. This will open up Visual Studio Code.
5. Again, confirm the "Allow an extension top open this URI" dialog.
6. It will ask you, where the everything should go.
   1. **It is important that the folder you choose does not have a space character anywhere!!**
   2. Choose a nice folder for it.
7. It will then start to download the repository. There is no progress bar unfortunately. Depending on your network connection, it might take some time. The whole thing is like 2GB!
8. As soon as it is done, it asks you, whether you want to open the repository.
9.  Confirm this. And also confirm that you trust the authors.

You should now see the files in the left panel. It should look similar to this:

![](img/after_pull.png)

## Step 3: Create the "environment"
We now need to install all the necessary software for the analysis. This comes in python packages that are installed in so-called "environments". An environment is basically a python interpreter with all the packages.

**IF YOU USE WINDOWS, YOU NEED TO DO THIS:**

1. Open the file called `environment.yml`
2. Find the line that reads: `- xorg-libxxf86vm`
3. Delete this line

In order to do this (no matter what OS you use), follow these steps:

1. In the Menu Bar Choose `Terminal` and the `New Terminal`
2. **Only on Windows**:
   1. You cannot use the Terminal that it opens
   2. On the top of the Terminal part, there is a bar with some Symbols
   3. One of these looks like this: ![](img/new_terminal_button.png)
   4. Click on the arrow pointing downwards and choose "Git Bash"
3. In the Terminal, enter:
4. `mamba env create -p ./.venv`

**This can take a long time. It probably needs to download 600-800MB and install it. Depending on your internet connection and the speed of your laptop, this might take more than one hour to complete!**

## Step 4: Did it work?
Let's see if it worked!

In the file panel, open the file `01_check_if_it_works.ipynb` by clicking on it.

It should open in the editor now and you should see a button that says: `Run All`. Click it.

It might ask you to choose a kernel. In that case choose the `.venv/bin/python` kernel on Mac and Linux and the `.venv/bin/python.exe` one on Windows.

It should start working now and after a minute or so, you should see a new window with a line going from the bottom left to the top right. The script should have also created some output.

## Step 5: What now?
If it worked, you are done. If it did not work, let me know as soon as possible. Tell me:

1. What step did not work?
2. All error messages you might have received.
3. Send me a screenshot of the problem.