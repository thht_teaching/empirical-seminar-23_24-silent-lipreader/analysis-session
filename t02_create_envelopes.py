#%% imports
import av
import numpy as np
from pathlib import Path
from th_analysis.audio.envelope import extract_envelope
from tqdm import tqdm

#%% set variables
input_path = Path('/home/th/git/experiments/autw_lipreading/attw_lipreading/stimuli/videos')
output_path = Path('data/envelopes')

output_path.mkdir(parents=True, exist_ok=True)

#%% extract envelopes
all_files = list(input_path.glob('*.mp4'))

for cur_file in tqdm(all_files):
    container = av.open(str(cur_file))

    audio_stream = next(s for s in container.streams if s.type == 'audio')

    # Create an empty list to collect audio frames
    frames = []

    for packet in container.demux(audio_stream):
        for frame in packet.decode():
            # Convert audio frame to numpy array
            frames.append(frame.to_ndarray())

    # Concatenate all frames into one numpy array
    audio_data = np.mean(np.concatenate(frames, axis=1), axis=0)

    audio_sfreq = audio_stream.rate

    envelope = extract_envelope(audio_data, frame_rate=audio_sfreq)
    envelope = envelope.get_data().squeeze()

    np.save(output_path / (cur_file.stem + '.npy'), envelope, allow_pickle=False)