from helpers.raw import Raw
from cluster_jobs.c01_decode import DecodeJob
from plus_slurm import ApptainerJobCluster, PermuteArgument

Raw.sinuhe_root = '/mnt/sinuhe/data_raw'

all_subjects = Raw.get_all_subjects()

job_cluster = ApptainerJobCluster(
    required_ram="8G",
    request_cpus=1,
    request_time=120,
    apptainer_image='oras://ghcr.io/thht/obob-singularity-container/xfce_desktop_minimal:latest',
    exclude_nodes='scs1-8, scs1-9, scs1-12, scs1-14'
)

#%% add jobs
job_cluster.add_job(DecodeJob,
                    subject_id=PermuteArgument(all_subjects),
                    block_nr=PermuteArgument([1, 2, 3, 4, 5, 6])
)

#%% submit
job_cluster.submit()
