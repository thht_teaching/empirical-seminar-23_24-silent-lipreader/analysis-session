from helpers.slurm import Job
from pathlib import Path
import mne
import numpy as np
from helpers.raw import Raw
from helpers.epochs import LipreadingEpochs
from tqdm import tqdm
import eelbrain as eb
import hashlib
import pandas as pd
import joblib

maxwell_kwargs = dict(
    calibration='/mnt/obob/staff/thartmann/experiments/markov-speech-online/analysis/assets/maxfilter_cal/sss_cal.dat',
    cross_talk='/mnt/obob/staff/thartmann/experiments/markov-speech-online/analysis/assets/maxfilter_cal/ct_sparse.fif',
)


class DecodeJob(Job):
    job_data_folder = '01_decode'


    def run(self, subject_id, block_nr):
        Raw.sinuhe_root = '/mnt/sinuhe/data_raw'
        
        raw = Raw(self.original_subject_id, block_nr)

        (ch_noise, ch_flat) = mne.preprocessing.find_bad_channels_maxwell(
            raw, **maxwell_kwargs
        )
        raw.info['bads'] = ch_noise + ch_flat

        destination = None

        raw = mne.preprocessing.maxwell_filter(
            raw,
            destination=destination,
            **maxwell_kwargs
        )
        
        all_epochs = [LipreadingEpochs(self.subject_id, raw, i).load_data().filter(l_freq=0.1, h_freq=10).resample(50).pick(['mag', 'misc']) for i in range(0, 8)]
        
        for epoch in tqdm(all_epochs):
            decoded_mouth_area = eb.boosting(
                x=epoch.eelbrain_meg_data,
                y=epoch.get_eelbrain_from_channel('mouth_area'),
                tstart=-.4,
                tstop=.1,
                partitions=5,
                test=1
            )

            decoded_envelope = eb.boosting(
                x=epoch.eelbrain_meg_data,
                y=epoch.get_eelbrain_from_channel('envelope'),
                tstart=-.4,
                tstop=.1,
                partitions=5,
                test=1
            )

            epoch.metadata['r_mouth_area'] = decoded_mouth_area.r
            epoch.metadata['r_envelope'] = decoded_envelope.r
            epoch.metadata['subject_id'] = self.subject_id

        combined_metadata = pd.concat([epoch.metadata for epoch in all_epochs])

        joblib.dump(all_epochs, self.full_output_path, compress=('xz', 4))
        combined_metadata.to_parquet(self.full_output_path.with_suffix('.metadata.parquet'))


        