from cluster_jobs.c01_decode import DecodeJob
from helpers.raw import Raw

Raw.sinuhe_root = '/mnt/sinuhe/data_raw'

subject_id = list(Raw.get_all_subjects())[0]

job = DecodeJob(subject_id=subject_id, block_nr=1)
job.run_private()