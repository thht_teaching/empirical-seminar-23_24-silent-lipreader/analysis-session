import numpy as np
import pandas as pd
from pymatreader import read_mat
from copy import deepcopy
from pathlib import Path
from scipy.signal import resample
from scipy.interpolate import PchipInterpolator
import mne
import eelbrain as eb

from obob_mne.mixins.raw import AdvancedEvents, LoadFromSinuhe, AutomaticBinaryEvents
from obob_mne.events import read_events_from_analogue

class EelbrainMixin:
    @property
    def eelbrain_time_dim(self):
        return eb.UTS(
            tmin=self.times.min(),
            tstep=1. / self.info['sfreq'],
            nsamples=self.times.shape[0]
        )

    @property
    def eelbrain_meg_sensor_dim(self):
        meg_channel_names = [x['ch_name'] for x in self.info['chs'] if
                             x['ch_name'].startswith('MEG')]
        meg_channel_locations = [x['loc'][:3] for x in self.info['chs']
                                 if x['ch_name'].startswith('MEG')]

        return eb.Sensor(meg_channel_locations, meg_channel_names)

    @property
    def eelbrain_meg_data(self):
        meg_data = self.copy().load_data().pick('meg').get_data()
        return eb.NDVar(
            meg_data,
            (self.eelbrain_meg_sensor_dim, self.eelbrain_time_dim),
            name='brain'
        )

    def get_eelbrain_from_channels(self, channels):
        data = self.get_data(channels)
        return eb.NDVar(
            data,
            (eb.Categorial('/'.join(channels), channels), self.eelbrain_time_dim)
        )



class Raw(LoadFromSinuhe, AdvancedEvents, EelbrainMixin):
    study_acronym = 'th_attw_lipreading'
    file_glob_patterns = ['%s_block%02d.fif']
    sinuhe_root = 'data/fif'
    mouth_area_fs = 25
    sound_envelope_fs = 44100

    def __init__(self, subject_id, *args, **kwargs):
        self.subject_id = subject_id
        super().__init__(subject_id, *args, **kwargs)

    def _load_events(self):
        self._event_id = dict()
        self._events = read_events_from_analogue(self, tolerance=3)

        self._process_events()

    def _process_events(self):
        tmp_metadata = list()
        for event in self._events:
            cur_entry = {
                'video_idx': event[2] & 0b00111111,
                'forward': event[2] & 0b01000000 > 0,
                'mandarin': event[2] & 0b10000000 > 0,
                'onset_sample': event[0]
            }
            l_code = 'M' if cur_entry['mandarin'] else 'D'
            tmp_data = np.load(Path('data', 'envelopes', f'{l_code}{cur_entry["video_idx"]:02}.npy'))
            new_length = int(tmp_data.shape[0] / self.sound_envelope_fs * self.info['sfreq'])
            cur_entry['duration_sample'] = new_length

            tmp_metadata.append(cur_entry)
        self._evt_metadata = pd.DataFrame(tmp_metadata)
        self._add_mouth_area()
        self._add_sound_envelopes()

    def _add_mouth_area(self):
        mouth_area = np.nan * np.ones(self.n_times)
        for cur_evt_onset, cur_metadata in zip(self.events[:, 0], self.evt_metadata.iterrows()):
            cur_evt_onset -= self.first_samp
            l_code = 'M' if cur_metadata[1]['mandarin'] else 'D'
            tmp_data = np.load(Path('data', 'mouth_areas', f'{l_code}{cur_metadata[1]["video_idx"]:02}.npy'))
            new_length = int(tmp_data.shape[0] / self.mouth_area_fs * self.info['sfreq'])
            tmp_data_resampled = PchipInterpolator(np.arange(tmp_data.shape[0]), tmp_data)(np.linspace(0, tmp_data.shape[0] - 1, new_length))
            if not cur_metadata[1]['forward']:
                tmp_data_resampled = tmp_data_resampled[::-1]
            assert np.all(np.isnan(mouth_area[cur_evt_onset:cur_evt_onset + new_length]))  # make sure mouth areas do not overlap
            mouth_area[cur_evt_onset:cur_evt_onset + new_length] = tmp_data_resampled
        
        mouth_area = np.nan_to_num(mouth_area)
        
        mouth_info = mne.create_info(['mouth_area'], 
                                     sfreq=self.info['sfreq'],
                                     ch_types=['misc'])
        
        mouth_raw = mne.io.RawArray(mouth_area[None, :], mouth_info)

        self.load_data()
        self.add_channels([mouth_raw], force_update_info=True)

    def _add_sound_envelopes(self):
        sound_env = np.nan * np.ones(self.n_times)
        for cur_evt_onset, cur_metadata in zip(self.events[:, 0], self.evt_metadata.iterrows()):
            cur_evt_onset -= self.first_samp
            l_code = 'M' if cur_metadata[1]['mandarin'] else 'D'
            tmp_data = np.load(Path('data', 'envelopes', f'{l_code}{cur_metadata[1]["video_idx"]:02}.npy'))
            new_length = int(tmp_data.shape[0] / self.sound_envelope_fs * self.info['sfreq'])
            tmp_data_resampled = resample(tmp_data, new_length)
            if not cur_metadata[1]['forward']:
                tmp_data_resampled = tmp_data_resampled[::-1]
            assert np.all(np.isnan(sound_env[cur_evt_onset:cur_evt_onset + new_length]))  # make sure mouth areas do not overlap
            sound_env[cur_evt_onset:cur_evt_onset + new_length] = tmp_data_resampled

        sound_env = np.nan_to_num(sound_env)
        
        sound_env_info = mne.create_info(['envelope'], 
                                     sfreq=self.info['sfreq'],
                                     ch_types=['misc'])
        
        sound_env_raw = mne.io.RawArray(sound_env[None, :], sound_env_info)
        sound_env_raw.filter(h_freq=30, l_freq=None, picks='misc')
        self.add_channels([sound_env_raw], force_update_info=True)



class RawSounds(AutomaticBinaryEvents):
    condition_triggers = {
        'condition_detuned': 1,
        'markov': {
            'random': 2,
            'ordered': 4
        }
    }

    stimulus_triggers = {
        'freq': {
            1: 16,
            2: 32,
            3: 64,
            4: 128
        },
        'tone_detuned': 1
    }