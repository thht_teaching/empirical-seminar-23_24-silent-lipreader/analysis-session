import mne
import eelbrain as eb
from pathlib import Path


class LipreadingEpochs(mne.Epochs):
    def __init__(self, subject_id, raw, trial_nr, *args, tmin=-1, tmax=1, **kwargs):
        self.subject_id = subject_id
        cur_metadata = raw.evt_metadata.iloc[trial_nr:trial_nr + 1]
        cur_events = raw.events[trial_nr:trial_nr + 1, :]
        tmax = tmax + cur_metadata['duration_sample'].values[0] / raw.info['sfreq']
        super().__init__(*args, 
                         raw=raw, 
                         events=cur_events,
                         metadata=cur_metadata,
                         tmin=tmin,
                         tmax=tmax,
                         **kwargs)

    @property
    def eelbrain_time_dim(self):
        return eb.UTS(
            tmin=self.times.min(),
            tstep=1./self.info['sfreq'],
            nsamples=self.times.shape[0]
        )

    @property
    def eelbrain_meg_sensor_dim(self):
        meg_channel_names = [x['ch_name'] for x in self.info['chs'] if
                             x['ch_name'].startswith('MEG')]
        meg_channel_locations = [x['loc'][:3] for x in self.info['chs']
                                 if x['ch_name'].startswith('MEG')]

        return eb.Sensor(meg_channel_locations, meg_channel_names)

    @property
    def eelbrain_meg_data(self):
        meg_data = self.copy().load_data().pick('meg').get_data()
        return eb.NDVar(
            meg_data[0, :, :],
            (self.eelbrain_meg_sensor_dim, self.eelbrain_time_dim),
            name='brain'
        )

    def get_eelbrain_from_channel(self, channel):
        data = self.get_data(channel)
        return eb.NDVar(
            data[0, 0, :],
            (self.eelbrain_time_dim,),
            name=channel
        )
