from plus_slurm import AutomaticFilenameJob
import hashlib


class Job(AutomaticFilenameJob):
    base_data_folder = 'data'

    def __init__(self, subject_id, *args, **kwargs):
        self.original_subject_id = subject_id
        subject_id = hashlib.sha256(subject_id.encode()).hexdigest()[:8]
        super().__init__(*args, subject_id=subject_id, **kwargs)