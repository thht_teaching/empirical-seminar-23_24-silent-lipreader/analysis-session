#%% imports
from pymatreader import read_mat
import numpy as np
import pandas as pd
from pathlib import Path
import hashlib

#%% set variables
input_file = Path('/home/th/git/experiments/autw_lipreading/attw_lipreading/behav.mat')

#%% load data
data = read_mat(input_file)['data']
# %% put in pandas
df = pd.DataFrame(data)

df['subject_id'] = df['subject_id'].apply(lambda x: hashlib.sha256(x.encode()).hexdigest()[:8])

#%% save
df.to_excel('data/behav.xlsx')
